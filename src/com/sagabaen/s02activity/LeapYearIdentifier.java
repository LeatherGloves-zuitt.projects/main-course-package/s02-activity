package com.sagabaen.s02activity;

import java.util.Scanner;

public class LeapYearIdentifier {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter Year: ");
        int year = Integer.parseInt(input.nextLine());
        boolean leap = false;
        if (year % 4 == 0) {
            if (year % 100 == 0) {
                if (year % 400 == 0){
                    leap = true;
                } else {
                    leap = false;
                }
            } else {
                leap = true;
            }
        }
        else{
            leap = false;
        }
        if (leap){
            System.out.println(year + " is a leap year.");
        }
        else {
            System.out.println(year + " is not a leap year.");
        }
    }
}
